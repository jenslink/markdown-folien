% Markdown
% Jens Link
% FLARP - 04.05.2023

## $ whomai

* Freelancer
* Linux seit es das auf 35 Disketten gab
* Netzwerkkram seit 2000
* IPv6 seit 2007
* Automatisierung
* mag die CLI
* Orga Adminstammtisch Berlin: [https://www.flarp.de](https://www.flarp.de))

## Markdown (0)

Heute reden wir über aktuelle Technologie \pause von 2004.

![](markdown-blog.png)

[https://daringfireball.net/projects/markdown/](https://daringfireball.net/projects/markdown/)


## Markdown (I)

>  Markdown ist eine vereinfachte Auszeichnungssprache [...]  Ziel von Markdown ist,
>  dass schon die Ausgangsform ohne weitere Konvertierung leicht lesbar ist.

[https://de.wikipedia.org/wiki/Markdown](https://de.wikipedia.org/wiki/Markdown)

## Markdown (II)

* Reine Textdateien
* einfach zu schreiben, einfach zu lesen
* flexibel bei den Ausgabeformaten
* Einfach versionierbar

## Markdown (III)


> The nice thing about standards is that you have so many
> to choose from; furthermore, if you do not like any of
> them, you can just wait for next year's model."
-- Andrew S. Tanenbaum (or Grace Hopper)


\pause

- Man spricht von *Flavors*
- Nicht alles wird überall unterstütz
- Manchmal sind Dinge anders

## Wozu?

* Präsentationen (So wie diese hier)
* Dokumentation in github / gitlab / ...
* Wikis
* Webseiten
* Briefe und Rechnungen
* Diagramme (via mermaid.js)

## Workflow

```mmd
markdown file --> Tool --> Output
```

\colA{6cm}
Tool z.B.:

* Browser Plugin
* Code auf Server Seite
* lokaler Konverter
\pause

\colB{6cm}

Output z.B.:

* HTML
* PDF
* .docx
* .pptx

\colEnd

## Tools (Bei mir)
* vim mit einigen Plugins
* git für die Versionskontrolle
* pandoc für
  * Präsentationen
  * Briefe / Rechnungen
  * andere Dokumente
* gohugo für Webseiten
* github / gitlab

## markdown DEMO

DEMO

## Auftritt pandoc

> If you need to convert files from one markup format into another,
> pandoc is your swiss-army knife.

[https://www.pandoc.org](https://pandoc.org)

## Folien (I)

``` term
$ pandoc <input>  -t beamer -o <output>.pdf
```

Tipp:
[https://kofler.info/folien-mit-pandoc-erstellen/](https://kofler.info/folien-mit-pandoc-erstellen/)

## Folien (II)

Oder, modern im Browser:

```
pandoc -t revealjs -s -o intro.html intro.md \
       -V revealjs-url=https://unpkg.com/reveal.js@3.9.2/
```

Tipp: [https://gisbers.de/posts/revealjs-pandoc/](https://gisbers.de/posts/revealjs-pandoc/)

## Texte

``` term
$ pandoc <input> -o <output>.pdf
```

\pause

oder lieber Word?

``` term
$ pandoc <input> -o <output>.docx
```

## Briefe

[Pandoc Letter Template (Default: DIN 5008)](https://github.com/benedictdudel/pandoc-letter-din5008)

## Pandoc DEMO

DEMO


## Webseiten mit markdown

- Selber HTML und CSS schreiben ist Arbeit
- CMS Systeme unterstützen (teilweise) Markdown
- Generatoren für statisch Webseiten gibt es mehr als Sand am Meer.
    - Statische Webseiten -> Weniger Sicherheistprobleme
    - grösstes Proble: Ein passendes Theme finden ;-)

## Auftritt Hugo

> The world’s fastest framework for building websites

[https://gohugo.io/](https://www.gohugo.io)

## Wer Hugo nicht mag

[https://jamstack.org/generators/](https://jamstack.org/generators/)

## Von XXXX nach Hugo

* [https://gohugo.io/tools/migrations/](https://gohugo.io/tools/migrations/)
* Für [flarp.de](https://www.flarp.de) den Wordpress Exporter
    * Es fällt ein .zip mit lauter Markdown Files raus
    * Diese kann man dann für Hugo verwenden
    * Vorteil: mit grep /sed lassen sich Altlasten schnell finden und beheben

## Hugo Demo

DEMO

## Github / Gitlab / ...

* Für eingentlich jedes Repository auf Github / Gitlab gibt es ein README.md
* über git lassen sich Workflows abbilden
* Im Markdown Kontext:
    * Gemeinsam an Dokumenten arbeiten
    * Vernünftige Versionierung (nicht: foo-05042023-01.docx)
    * ggf. Freigabeprozesse abbilden

## Beispiel: Firmen Blog (I)

* Jeder Mitarbeiter soll beitragen können
* Irgendwer trägt die Verantwortung und muss Artikel reviewen und freigeben

## Beispiel: Firmen Blog (II)

```term
> git clone blog
> cd blog
> git checkout -b markdown
...
> git add .
> git commit
> git push
```
## Beispiel: Firmen Blog (III)

* Review des Artikels
* wenn okay -> merge mit main Repo
* veröffentlichen

## Beispiel: Firmen Blog (IV)

**Tipp:** Grip is a command-line server application written in Python that uses the
GitHub markdown API to render a local readme file.

[https://github.com/joeyespo/grip](https://github.com/joeyespo/grip)

## gitlab Demo

## Und sonst?

Ist euer CV aktuell?

- [https://github.com/elipapa/markdown-cv](https://github.com/elipapa/markdown-cv)

## Danke!

Fragen?
